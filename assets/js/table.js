const people = [
  { name: "prof. Medrea Camelia", function: "Responsabil" },
  { name: "prof. Cozma Onesim", function: "Membru" },
  {
    name: "prof. Romaneţ Remus",
    function: "Membru - reprezentant sindicat",
  },
  { name: "prof. Rusu Carmen", function: "Membru" },
  { name: "înv. Martin Maria", function: "Membru" },
  { name: "prof. Fazecas Debora", function: "Membru" },
  { name: "prof. Hodisan Mirela", function: "Membru" },
  { name: "prof. Chichinejdi Eugenia", function: "Membru" },
  {
    name: "Apolzan Liviu",
    function: "Membru - Reprezentant autoritate locala",
  },
  {
    name: "Vasilescu Daniela",
    function: "Membru - Reprezentant parinti",
  },
  { name: "Trifan Anamaria", function: "Membru - Reprezentant elevi" },
  {
    name: "Curpaş Florentina",
    function: "Membru- Responsabil scoala postliceala",
  },
];
var tableB = document.getElementById("table-body");
if (tableB) {
  for (let i = 0; i < people.length; i++) {
    let tr = document.createElement("tr");
    tableB.appendChild(tr);

    let name = document.createElement("td");
    let funct = document.createElement("td");
    name.classList.add("fw-bold");
    name.innerHTML = people[i].name;
    funct.innerHTML = people[i].function;
    tr.appendChild(name);
    tr.appendChild(funct);
  }
}
//
