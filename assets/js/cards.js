function createCardList() {
  var list = [];
  var ammOnPage = 12;

  flipPageF = () => {
    if (show[1] != list.length) show[0] = show[1];
    if (list.length <= show[1] + ammOnPage) show[1] = list.length;
    else show[1] += ammOnPage;
    showCards(show[0], show[1], list);
  };
  flipPageB = () => {
    if (show[1] - (show[1] - show[0]) > 0) show[1] -= show[1] - show[0];
    if (show[0] - ammOnPage >= 0) show[0] -= ammOnPage;

    showCards(show[0], show[1], list);
  };
  for (let i = 0; i < 30; i++) {
    list.push({
      cName: `Anunt ${i + 1}`,
      cText:
        "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nemo, quae et. Numquam natus sunt non dolorem ea eligendi accusamus qua",
    });
  }
  var show;
  if (list.length <= ammOnPage) show = [0, list.length];
  else show = [0, ammOnPage];
  showCards(show[0], show[1], list);
}

function showCards(start, end, list) {
  let row = document.getElementById("cardRow");
  if (row) row.remove();

  let rowCopy = document.createElement("div");
  rowCopy.id = "cardRow";
  rowCopy.classList.add("row");

  let cardRowContainer = document.getElementById("cardRowContainer");
  cardRowContainer.appendChild(rowCopy);

  let divArround3 = document.createElement("div");
  divArround3.classList.add("d-flex", "flex-wrap", "justify-content-evenly");

  let divArround2 = document.createElement("div");
  divArround2.classList.add("flex-fill");

  let divArround = document.createElement("div");
  divArround.classList.add("d-flex");

  let btnR = document.createElement("button");
  btnR.classList.add("flex-fill", "btn", "shadow-none");
  let iconR = document.createElement("i");
  iconR.style.fontSize = "1rem";
  iconR.classList.add("bi", "bi-chevron-right");
  btnR.appendChild(iconR);
  btnR.addEventListener("click", () => {
    flipPageF();
  });

  let btnL = document.createElement("button");
  btnL.classList.add("flex-fill", "btn", "shadow-none");
  let iconL = document.createElement("i");
  iconL.style.fontSize = "1rem";
  iconL.classList.add("bi", "bi-chevron-left");
  btnL.appendChild(iconL);
  btnL.addEventListener("click", () => {
    flipPageB();
  });

  divArround.appendChild(btnL);
  divArround.appendChild(divArround2);
  divArround2.appendChild(divArround3);
  rowCopy.appendChild(divArround);

  for (let i = start; i < end; i++) {
    let card = document.createElement("div");
    card.classList.add("card", "mb-2");
    card.style.width = "18rem";
    divArround3.appendChild(card);

    let cardB = document.createElement("div");
    cardB.classList.add("card-body");
    card.appendChild(cardB);

    let cardTi = document.createElement("h5");
    cardTi.classList.add("card-title", "anunt", "anunt--normal");
    cardTi.innerHTML = list[i].cName;
    cardB.appendChild(cardTi);

    let cardTe = document.createElement("p");
    cardTe.classList.add("card-text");
    cardTe.innerHTML = list[i].cText;
    cardB.appendChild(cardTe);

    let cardBtn = document.createElement("a");
    cardBtn.classList.add("btn", "btn-secondary", "float-end");
    cardBtn.innerHTML = "more..";
    cardBtn.href = "#";
    cardB.appendChild(cardBtn);
  }
  divArround.appendChild(btnR);
}
if (document.getElementById("cardRowContainer")) {
  createCardList();
}
