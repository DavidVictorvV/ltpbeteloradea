if (document.getElementById("mynav")) {
  var links = [
    "Acasa",
    "Despre noi",
    "Academic",
    "Inscrieri",
    "Documente",
    "Campus",
    "Stiri",
    "Contact",
  ];

  links.forEach((el) => {
    //create list item
    var currentPage = document.getElementById("currentPage");
    var linkList = document.getElementById("linkList");
    var lItem = document.createElement("li");
    lItem.classList.add("nav-item");
    var aItem = document.createElement("a");
    //assign data
    if (currentPage && linkList) {
      aItem.classList.add("nav-link");
      aItem.ariaCurrent = "page";
      if (el === "Acasa") {
        aItem.href = `./index.html`;
      } else if (el === "Despre noi") {
        aItem.href = `./despre-noi.html`;
      } else aItem.href = `./${el.toLocaleLowerCase()}.html`;
      aItem.innerHTML = el;

      //check if item is the linked one
      var path = window.location.pathname.split("/");
      if (el === "Acasa")
        if (path[3] === `index.html`) {
          aItem.classList.add("active");
          currentPage.innerText = el;
        }
      if (el === "Despre noi")
        if (path[3] === `despre-noi.html`) {
          aItem.classList.add("active");
          currentPage.innerText = el;
        }
      if (path[3] === `${el.toLocaleLowerCase()}.html`) {
        aItem.classList.add("active");
        currentPage.innerText = el;
      }
      lItem.appendChild(aItem);
      linkList.appendChild(lItem);
    }
  });

  //for when navbar opens on phone/tablet
  var navOpen = false;
  function changeRounded() {
    if (!navOpen) navOpen = true;
    else navOpen = false;

    var navBar = document.getElementById("mynav");
    var currentPage = document.getElementById("currentPage");
    if (navBar.classList.contains("rounded-pill")) {
      navBar.classList.toggle("justify-content-between");
      navBar.classList.toggle("justify-content-end");
      currentPage.classList.toggle("visually-hidden");
      if (navOpen) {
        navBar.classList.remove("rounded-pill");
        navBar.classList.add("rounded-3");
      }
    } else {
      setTimeout(() => {
        navBar.classList.toggle("justify-content-between");
        navBar.classList.toggle("justify-content-end");
        currentPage.classList.toggle("visually-hidden");
        if (!navOpen && $(this).scrollTop() < 100 / 2) {
          navBar.classList.add("rounded-pill");
          navBar.classList.remove("rounded-3");
        }
      }, 300);
    }
  }

  //for when navbar resize on web
  $(function () {
    var navBar = document.getElementById("mynav");
    var scrollUpBtn = document.getElementById("scrollUpBtn");
    var size = "big";
    headerHeight = 100;

    $(window).resize(function () {
      if ($(window).width() < 960) {
        size = "small";
        navBar.classList.remove("rounded-pill");
        navBar.classList.add("rounded-3");
      } else {
        size = "big";
        navBar.classList.add("rounded-pill");
        navBar.classList.remove("rounded-3");
      }
    });

    resizeNavbar();

    function resizeNavbar() {
      if (size == "big") {
        if ($(this).scrollTop() > headerHeight / 2) {
          if (navBar.classList.contains("container")) {
            navBar.classList.remove("container");
            navBar.classList.add("container-fluid");
            navBar.classList.add("rounded-3");
            navBar.classList.remove("rounded-pill");
            navBar.classList.toggle("navBarColor");
            navBar.classList.toggle("navBarColor-blur");
          }
          if (scrollUpBtn.classList.contains("visually-hidden"))
            scrollUpBtn.classList.remove("visually-hidden");
        } else {
          if (navBar.classList.contains("container-fluid")) {
            navBar.classList.add("container");
            navBar.classList.remove("container-fluid");
            navBar.classList.remove("rounded-3");
            navBar.classList.add("rounded-pill");
            navBar.classList.toggle("navBarColor");
            navBar.classList.toggle("navBarColor-blur");
          }
          if (!scrollUpBtn.classList.contains("visually-hidden"))
            scrollUpBtn.classList.add("visually-hidden");
        }
      }
    }
    $(window).scroll(() => {
      resizeNavbar();
    });
  });

  //scroll up button
  function scrollUp() {
    window.scrollTo(0, 0);
  }
}
